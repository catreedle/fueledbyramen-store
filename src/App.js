import React from 'react';
import Header from './common/Header';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LandingPage from './components/LandingPage';
import AllProducts from './components/product/AllProducts';
import Product from './components/product/Product';
import AllArtists from './components/user/AllArtists';
import Artist from './components/user/Artist';
import DeleteReview from './components/review/DeleteReview';
import NoMatch from './components/NoMatch'

function App() {
  return (
    <div>
      <Router>
        <Header />
        <Switch>
          <Route exact path='/' component={LandingPage} />
          <Route exact path='/artist' component={AllArtists} />
          <Route path='/artist/:id' component={Artist} />
          <Route exact path='/product' component={AllProducts} />
          <Route path='/product/:id' component={Product} />
          <Route path='/review/:id' component={DeleteReview} />
          <Route component={NoMatch}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
