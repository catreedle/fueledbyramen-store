import React from 'react';
import { withRouter } from 'react-router-dom'
import { Nav, NavLink } from 'reactstrap';
import Login from '../components/user/Login';
import Signup from '../components/user/Signup';
import CreateProduct from '../components/product/CreateProduct';

class Header extends React.Component {
    constructor(props) {
        super(props)
        this.logout = this.logout.bind(this)
    }

    logout() {
        localStorage.clear()
        this.props.history.push('/')
    }

    render() {
        var SignupCheck
        var LoginCheck

        if(localStorage.getItem("isLoggedin")) {
            LoginCheck = <NavLink href='#' onClick={this.logout}>Logout</NavLink>
            SignupCheck = <div></div>

        } else {
            LoginCheck = <Login />
            SignupCheck = <Signup />
        }
        return (
            <div>
                <div> <img src={require('../public/image/ramen.png')} width="100px" alt="ramen" /><a href="/">FUELEDBYRAMEN</a> </div>
                <Nav>
                    {LoginCheck}
                    {SignupCheck}
                    <NavLink href="/artist">Artists</NavLink>
                    <NavLink href="/product">Products</NavLink>
                    <CreateProduct />
                    {localStorage.getItem('isLoggedin') ? <NavLink href={`/artist/${localStorage.getItem('USER')}`}>Me</NavLink> : <div></div>}
                </Nav>
                <hr />
            </div>
        );
    }
}

export default withRouter(Header)