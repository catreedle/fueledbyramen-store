import React, { createRef } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Input, Label } from 'reactstrap';

import Dropzone from 'react-dropzone'
import axios from 'axios'

var dropzoneRef = createRef()

class UpdateProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            name: '',
            price: '',
            image: '',
            src: "https://image.flaticon.com/icons/png/512/3/3901.png",
            redirect: false
        };
        this.toggle = this.toggle.bind(this);
        this.submitProduct = this.submitProduct.bind(this)
        this.onChangeName = this.onChangeName.bind(this)
        this.onChangePrice = this.onChangePrice.bind(this)
    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    onChangeName(event) {
        this.setState({
            name: event.target.value
        })
    }
    onChangePrice(event) {
        this.setState({
            price: event.target.value
        })
    }
    submitProduct(event) {
        event.preventDefault()
        fetch(`https://binarmarketplace.herokuapp.com/product/${this.props.productId}`, {
            method: 'PUT',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
                name: this.state.name,
                price: this.state.price,
                image: this.state.image
            })
        }).then(response => response.json())
            .then(() => {
                window.location.reload()
            }).catch(function (error) {
                console.log(error)
            })
    }

    componentDidMount() {
        fetch(`https://binarmarketplace.herokuapp.com/product/${this.props.productId}`)
            .then(response => response.json())
            .then((data) => {
                this.setState(() => ({
                    name: data.data.name,
                    price: data.data.price,
                    image: data.data.image
                }))
            })

    }

    handleUploadImages = images => {
        // uploads is an array that would hold all the post methods for each image to be uploaded, then we'd use axios.all()
        const uploads = images.map(image => {
            // our formdata
            const formData = new FormData();
            formData.append("file", image);
            formData.append("tags", 'PRODUCT_IMAGE'); // Add tags for the images - {Array}
            formData.append("upload_preset", "marketplace"); // Replace the preset name with your own
            formData.append("api_key", "171698985145518"); // Replace API key with your own Cloudinary API key
            formData.append("timestamp", (Date.now() / 1000) | 0);

            // Replace cloudinary upload URL with yours
            return axios.post(
                "https://api.cloudinary.com/v1_1/catreedle/image/upload",
                formData,
                { headers: { "X-Requested-With": "XMLHttpRequest" } })
                .then(response =>
                    this.setState({
                        src: response.data.secure_url,
                        image: response.data.secure_url
                    })
                )
        });

        // We would use axios `.all()` method to perform concurrent image upload to cloudinary.
        axios.all(uploads).then(() => {
            // ... do anything after successful upload. You can setState() or save the data
            console.log('Images have all being uploaded')
        });
    }

    render() {

        return (
            <div>
                <Button onClick={this.toggle}>{this.props.buttonLabel}Update Product</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Update Product</ModalHeader>
                    <ModalBody>
                        <Form inline>
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Input onChange={this.onChangeName} type="text" value={this.state.name} name="name" placeholder="name" />
                            </FormGroup>
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Input onChange={this.onChangePrice} type="number" value={this.state.price} name="price" placeholder="price" />
                            </FormGroup>
                            <FormGroup>
                                <Dropzone ref={dropzoneRef} onDrop={this.handleUploadImages}>
                                    {({ getRootProps, getInputProps }) => (
                                        <div {...getRootProps()}>
                                            <input {...getInputProps()} />
                                            <img style={{ width: 100 }} src={this.state.src} alt="product" />
                                        </div>
                                    )}
                                </Dropzone>
                                <Label>Upload Image</Label>
                            </FormGroup>
                            {/* <Button>Submit</Button> */}
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.submitProduct}>Submit</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default UpdateProduct;