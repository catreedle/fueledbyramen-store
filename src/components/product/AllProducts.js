import React from "react"
import { ListGroup, ListGroupItem } from "reactstrap"

class AllProducts extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            products: []
        }
    }
    componentDidMount() {
        fetch("https://binarmarketplace.herokuapp.com/product")
            .then(response => response.json())
            .then((data) => {
                this.setState(state => ({
                    products: [...state.products, ...data.data]
                }))
            }).catch((error) => {
                console.log(error)
            })
    }

    render() {
        var listOfProducts = this.state.products.map(product => {
            return (
                <div key={product._id}>
                    <ListGroupItem>
                        <img src={product.image} width="150vw" alt="product" />
                        <a href={"/product/" + product._id}>{product.name}</a>
                        <p>Price: {product.price}</p>
                    </ListGroupItem>
                </div>
            )
        })

        return (
            <div>
                <h2>All Products</h2>
                <ListGroup>
                    {listOfProducts}
                </ListGroup>
            </div>
        )
    }
}

export default AllProducts