import React from "react"
import { ListGroup, ListGroupItem, Row, Col, Button } from 'reactstrap'
import AddReview from '../review/AddReview'
import ShowReviews from "../review/ShowReviews"
import UpdateProduct from "./UpdateProduct"

class Product extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            product: {},
            user: '',
            productId: this.props.match.params.id,
        }
        this.deleteProduct = this.deleteProduct.bind(this)
    }

    deleteProduct() {
        fetch(`https://binarmarketplace.herokuapp.com/product/${this.state.productId}`, {
            method: 'DELETE',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
                product: this.state.product
            })
        }).then(response => response.json())
            .then(() => {
                window.location.reload()
            }).catch((error) => {
                console.log(error)
            })
    }

    async componentDidMount() {
        try {
            const response = await fetch(`https://binarmarketplace.herokuapp.com/product/${this.props.match.params.id}`)
            const data = await response.json()
            if (data.success === false) {
                this.setState({
                    product: null
                })
            } else {
                this.setState({
                    product: data.data,
                    user: data.data.user,
                })
            }
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        if (this.state.product) {
            return (
                <div>
                    <h2>Product</h2>
                    <Row>
                        <Col md="4">
                            <ListGroup>
                                <ListGroupItem>
                                    <img src={this.state.product.image} width="200vw" alt="product" />
                                </ListGroupItem>
                            </ListGroup>
                        </Col>
                        <Col md="8">
                            <ListGroup>
                                <ListGroupItem>Name: {this.state.product.name}</ListGroupItem>
                                <ListGroupItem>Price: {this.state.product.price}</ListGroupItem>
                                <ListGroupItem >Artist: <a href={"/artist/" + this.state.user._id}>{this.state.user.username}</a></ListGroupItem>
                                <ListGroupItem>
                                    <Row>
                                        <Col md="4">
                                            {(localStorage.getItem('USER') === this.state.user._id) ? <Button onClick={this.deleteProduct}>Delete</Button> : <div></div>}
                                        </Col>
                                        <Col md="4">
                                            {(localStorage.getItem('USER') === this.state.user._id) ? <UpdateProduct productId={this.state.productId} /> : <div></div>}
                                        </Col>
                                        <Col md="4">
                                            {localStorage.getItem('isLoggedin') ? <AddReview productId={this.state.productId} /> : <div></div>}
                                        </Col>
                                    </Row>
                                </ListGroupItem>
                            </ListGroup>

                        </Col>
                    </Row>
                    <ShowReviews productId={this.state.productId} />
                </div>
            )
        } else {
            return (
                <div>
                    <h2>Product not found or already deleted</h2>
                </div>
            )
        }
    }
}

export default Product