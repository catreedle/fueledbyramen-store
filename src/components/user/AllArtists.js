import React from "react"
import { ListGroup, ListGroupItem } from "reactstrap"

class AllArtists extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            users: []
        }
    }
    componentDidMount() {
        fetch("https://binarmarketplace.herokuapp.com/user")
            .then(response => response.json())
            .then((data) => {
                this.setState(state => ({
                    users: [...state.users, ...data.data]
                }))
            }).catch((error) => {
                console.log(error)
            })
    }

    render() {
        var listOfUsers = this.state.users.map(user => {
            return (
                <div key={user._id}>
                    <ListGroupItem>
                        <div>
                            <img src={user.image} width="200vw" alt="user" />
                        </div>
                        <a href={"/artist/" + user._id}>{user.username}</a>
                    </ListGroupItem>
                </div>
            )
        })

        return (
            <div>
                <h2>All Artists</h2>
                <ListGroup>
                    {listOfUsers}
                </ListGroup>
            </div>
        )
    }
}

export default AllArtists