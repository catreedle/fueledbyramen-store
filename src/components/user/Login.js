import React from 'react'
import { NavLink, Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Input } from 'reactstrap';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            username: '',
            password: '',
            redirect: false
        };

        this.toggle = this.toggle.bind(this);
        this.Login = this.Login.bind(this)
        this.onChangeUsername = this.onChangeUsername.bind(this)
        this.onChangePassword = this.onChangePassword.bind(this)
    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    onChangeUsername(event) {
        this.setState({
            username: event.target.value
        })
    }
    onChangePassword(event) {
        this.setState({
            password: event.target.value
        })
    }
    Login(event) {
        event.preventDefault()
        fetch("https://binarmarketplace.herokuapp.com/user/login", {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
            })
        }).then(response => response.json())
        .then(data => {
            if(data.success) {
                localStorage.setItem("isLoggedin", true)
                localStorage.setItem('TOKEN', data.data.token)
                localStorage.setItem('USER', data.data.id)
            } else (
                alert('Fail to log in!')
            )
            
        }).then(() => {
            window.location.reload()
        }).catch((error) => {
            console.log(error)
        })
    }
    render() {
        
        return (
            <div>
                <NavLink href="#" onClick={this.toggle}>{this.props.buttonLabel}Login</NavLink>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Login</ModalHeader>
                    <ModalBody>
                        <Form inline>
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Input onChange={this.onChangeUsername}  type="text" name="username" placeholder="username" required />
                            </FormGroup>
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Input onChange={this.onChangePassword} type="password" name="password" placeholder="password" required />
                            </FormGroup>
                            {/* <Button>Submit</Button> */}
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.Login}>Submit</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Login;