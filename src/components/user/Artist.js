import React from "react"
import { ListGroup, ListGroupItem, Row, Col, Button } from 'reactstrap'
import UpdateArtist from "./UpdateArtist"

class Artist extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            products: []
        }
        this.deleteArtist = this.deleteArtist.bind(this)
    }

    deleteArtist() {
        fetch(`https://binarmarketplace.herokuapp.com/user/${this.props.match.params.id}`, {
            method: 'DELETE',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
                user: this.state.user
            })
        }).then(response => response.json())
            .then(() => {
                localStorage.clear()
                window.location.reload()
            }).catch((error) => {
                console.log(error)
            })
    }

    async componentDidMount() {
        try {
            const response = await fetch(`https://binarmarketplace.herokuapp.com/user/${this.props.match.params.id}`)
            const data = await response.json()
            if (data.success === false) {
                this.setState({
                    user: null
                })
            } else {
                this.setState({
                    user: data.data,
                    products: data.data.products
                })
            }
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        if (this.state.user) {
            return (
                <div>
                    <h2>Artist</h2>
                    <Row>
                        <Col md="4">
                            <ListGroup>
                                <ListGroupItem>
                                    <img src={this.state.user.image} width="400vw" alt="user" />
                                </ListGroupItem>
                            </ListGroup>
                        </Col>
                        <Col md="8">
                            <ListGroup>
                                <ListGroupItem>Name: {this.state.user.username}</ListGroupItem>
                                <ListGroupItem>Email: {this.state.user.email}</ListGroupItem>
                                <ListGroupItem>
                                    <Row>
                                        <Col md="6">
                                            {(localStorage.getItem('USER') === this.state.user._id) ? <Button onClick={this.deleteArtist}>Delete</Button> : <div></div>}
                                        </Col>
                                        <Col md="6">
                                            {(localStorage.getItem('USER') === this.state.user._id) ? <UpdateArtist userId={this.state.user._id} /> : <div></div>}
                                        </Col>
                                    </Row>
                                </ListGroupItem>
                            </ListGroup>
                        </Col>
                    </Row>
                    <hr />
                    <h5>Products by this artist</h5>
                    {this.state.products[0] ? <div>{this.state.products.map((product) => (
                        <ListGroup>
                            <ListGroupItem>
                                <div><img src={product.image} width="100px" alt="product" /></div>
                                <a href={'/product/' + product._id}>{product.name} </a>
                                <p>{product.price}</p>
                            </ListGroupItem>
                        </ListGroup>
                    ))}</div> : <div>No products listed</div>}
                </div>
            )
        } else {
            return (
                <div>
                    <h2>Artist not found</h2>
                </div>
            )
        }
    }
}

export default Artist