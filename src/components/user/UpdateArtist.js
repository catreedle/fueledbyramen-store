import React, { createRef } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Input, Label, Col } from 'reactstrap';
import Dropzone from 'react-dropzone'
import axios from 'axios'
var dropzoneRef = createRef()

class UpdateArtist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            username: '',
            email: '',
            password: '',
            image: '',
            src: "https://image.flaticon.com/icons/png/512/3/3901.png",
            redirect: false
        };
        this.toggle = this.toggle.bind(this);
        this.UpdateArtist = this.UpdateArtist.bind(this)
        this.onChangeUsername = this.onChangeUsername.bind(this)
        this.onChangeEmail = this.onChangeEmail.bind(this)
        this.onChangePassword = this.onChangePassword.bind(this)

    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    onChangeUsername(event) {
        this.setState({
            username: event.target.value
        })
    }
    onChangeEmail(event) {
        this.setState({
            email: event.target.value
        })
    }
    onChangePassword(event) {
        this.setState({
            password: event.target.value
        })
    }
    UpdateArtist(event) {
        event.preventDefault()
        fetch(`https://binarmarketplace.herokuapp.com/user/${this.props.userId}`, {
            method: 'PUT',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
                username: this.state.username,
                email: this.state.email,
                image: this.state.image
            })
        }).then(response => response.json())
            // .then(data => {
            //     localStorage.setItem("isLoggedin", false)
            //     localStorage.setItem('TOKEN', data.data.token)
            //     localStorage.setItem('USER', data.data.id)
            // })
            .then(() => {
                window.location.reload()
            }).catch((error) => {
                console.log(error)
            })
    }

    componentDidMount() {
        fetch(`https://binarmarketplace.herokuapp.com/user/${this.props.userId}`)
            .then(response => response.json())
            .then((data) => {
                this.setState(() => ({
                    username: data.data.username,
                    email: data.data.email,
                    image: data.data.image
                }))
            })
    }

    handleUploadImages = images => {
        // uploads is an array that would hold all the post methods for each image to be uploaded, then we'd use axios.all()
        const uploads = images.map(image => {
            // our formdata
            const formData = new FormData();
            formData.append("file", image);
            formData.append("tags", 'PRODUCT_IMAGE'); // Add tags for the images - {Array}
            formData.append("upload_preset", "marketplace"); // Replace the preset name with your own
            formData.append("api_key", "171698985145518"); // Replace API key with your own Cloudinary API key
            formData.append("timestamp", (Date.now() / 1000) | 0);

            // Replace cloudinary upload URL with yours
            return axios.post(
                "https://api.cloudinary.com/v1_1/catreedle/image/upload",
                formData,
                { headers: { "X-Requested-With": "XMLHttpRequest" } })
                .then(response =>
                    this.setState({
                        src: response.data.secure_url,
                        image: response.data.secure_url
                    })
                )
        });

        // We would use axios `.all()` method to perform concurrent image upload to cloudinary.
        axios.all(uploads).then(() => {
            // ... do anything after successful upload. You can setState() or save the data
            console.log('Images have all being uploaded')
        });
    }

    render() {
        return (
            <div>
                <Button onClick={this.toggle}>{this.props.buttonLabel}Update</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>UpdateArtist</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup row>
                                <Label sm={2}>Username</Label>
                                <Col sm={10}>
                                    <Input onChange={this.onChangeUsername} type="text" name="username" value={this.state.username} placeholder="username" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={2}>Email</Label>
                                <Col sm={10}>
                                    <Input onChange={this.onChangeEmail} type="email" name="email" value={this.state.email} placeholder="email" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={2}>Password</Label>
                                <Col sm={10}>
                                    <Input onChange={this.onChangePassword} type="password" name="password" placeholder="password" />
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Dropzone ref={dropzoneRef} onDrop={this.handleUploadImages}>
                                    {({ getRootProps, getInputProps }) => (
                                        <div {...getRootProps()}>
                                            <input {...getInputProps()} />
                                            <img style={{ width: 100 }} src={this.state.src} alt="user" />
                                        </div>
                                    )}
                                </Dropzone>
                                <Label>Upload Image</Label>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.UpdateArtist}>Submit</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default UpdateArtist;