import React, { Component } from 'react'
import ReactPlayer from 'react-player'
 
class LandingPage extends Component {
  render () {
    return (
        <div className='player-wrapper'>
          <ReactPlayer
            className='react-player'
            url='https://www.youtube.com/watch?v=hMAPyGoqQVw'
            width='100vw'
            height='100vh'
            playing
            muted
            loop
          />
        </div>
      )
  }
}

export default LandingPage