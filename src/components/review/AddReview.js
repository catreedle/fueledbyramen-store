import React from 'react';
import { UncontrolledCollapse, Button, CardBody, Card, Form, FormGroup, Input, Label } from 'reactstrap';

class AddReview extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            description: ''
        }
        this.onChangeDescription = this.onChangeDescription.bind(this)
        this.submitReview = this.submitReview.bind(this)
    }

    onChangeDescription(event) {
        this.setState({
            description: event.target.value
        })
    }
    submitReview() {
        fetch(`https://binarmarketplace.herokuapp.com/review?productid=${this.props.productId}`, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
                description: this.state.description
            })
        }).then(response => response.json())
            .then(() => {
                window.location.reload()
            }).catch((error) => {
                console.log(error)
            })
    }

    render() {
        return (
            <div>
                <Button color="primary" id="toggler" style={{ marginBottom: '1rem' }}>
                    Add Review</Button>
                <UncontrolledCollapse toggler="#toggler">
                    <Card>
                        <CardBody>
                            <Form>
                                <FormGroup>
                                    <Label>Review</Label>
                                    <Input onChange={this.onChangeDescription} type="textarea" name="description" placeholder="add in your review about this product" />
                                </FormGroup>
                                <Button onClick={this.submitReview}>Submit</Button>
                            </Form>
                        </CardBody>
                    </Card>
                </UncontrolledCollapse>
            </div>
        )
    }
}

export default AddReview;