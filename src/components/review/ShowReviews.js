import React from "react"
import { Button, ListGroup, ListGroupItem } from "reactstrap"

class ShowReviews extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reviews: []
        }
    }

    componentDidMount() {
        fetch(`https://binarmarketplace.herokuapp.com/product/${this.props.productId}`)
            .then(response => response.json())
            .then((data) => {
                this.setState({
                    reviews: data.data.reviews
                })
            }).catch((error) => {
                console.log(error)
            })
    }

    render() {
        if (this.state.reviews[0]) {
            return (
                <div>
                    <hr />
                    <h5>Reviews</h5>
                    {this.state.reviews.map((review) => (<ListGroup>
                        <div key={review._id} >
                            <ListGroupItem>
                                <a href={"/artist/" + review.user._id}>{review.user.username}</a>
                                <p>{review.description}</p>
                                {review.user._id === localStorage.getItem('USER') ? <Button href={`/review/${review._id}`}>Delete</Button> : <div></div>}
                            </ListGroupItem>

                        </div>
                    </ListGroup>))}
                </div>
            )
        } else {
            return (
                <div></div>
            )
        }
    }
}
export default ShowReviews