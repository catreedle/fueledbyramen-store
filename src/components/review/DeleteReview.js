import React from "react"
import { Redirect } from 'react-router-dom'

class DeleteReview extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            review: {},
            redirect: false
        }
    }

    componentDidMount() {
        fetch(`https://binarmarketplace.herokuapp.com/review/${this.props.match.params.id}`, {
            method: 'DELETE',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${localStorage.getItem('TOKEN')}`
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
                review: this.state.review
            })
        }).then(response => response.json())
        .then(() => {
            this.setState({
                redirect: true
            })
        }).catch((error) => {
            console.log(error)
        })
    }

    render() {
        if(this.state.redirect) {
            return (
                <Redirect to={`/product/`} />
            )
        } else {
            return (
                <div>
                    Deleting review...
                </div>
            )
        }
    }
}

export default DeleteReview